import React from "react";
import ProjectCard from "../components/ProjectCard";
import project from "../data/project.json";
import Resource from "./Resource";

function Research() {
  return (
    <div className="container-fluid">
      <div className="px-lg-5">
        <div className="container">
          <div className="col-lg-12 mb-1 mb-sm-0">
            <span class="section-title text-primary mb-3 mb-sm-4">
              Current Research
            </span>
          </div>
        </div>
      </div>

      <section id="project" class="project">
        <div class="container">
          <div class="row">
            {project.projects.map((p, i) => {
              return <ProjectCard project={p} key={i} />;
            })}
          </div>
        </div>
      </section>
      <Resource />
    </div>
  );
}

export default Research;
