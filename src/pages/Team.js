import React from "react";
import team from "../data/team.json";
import TeamCard from "../components/TeamCard";

function Team() {
  return (
    <div className="backGround">
      <div className="container-fluid">
        <div className="px-lg-5">
          <div className="container">
            <div className="col-lg-12 mb-1 mb-sm-0">
              <span class="section-title text-primary mb-3 mb-sm-4">
                Members
              </span>
            </div>
            {/* <div class="col-lg-12 mx-auto">
              <div class="text-white p-5 shadow-sm rounded banner">
                <h1 class="display-2">Members</h1>
                <p class="lead">Current members.</p>
              </div>
            </div> */}
          </div>
          <div className="row">
            {team.team.map((t, i) => {
              return <TeamCard team={t} key={i} />;
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Team;
