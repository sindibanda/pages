import React from "react";
import About from "./About";
import Kerby from "./Kerby";
import Research from "./Research";

function Home() {
  return (
    <div>
      <section id="hero" class="d-flex align-items-center">
        <div
          class="container position-relative"
          data-aos="fade-up"
          data-aos-delay="500"
        >
          <h1>Welcome to CEADS Cyber Nuke</h1>
          <h2>
            Idaho State University CEADS efforts in nuclear AI and cybersecurity
          </h2>
          {/* <a href="#about" class="btn-get-started scrollto">
            Get Started
          </a> */}
        </div>
      </section>
      <About />
      <Kerby />
      <Research />
    </div>
  );
}

export default Home;
