import React from "react";
import ResourceData from "../data/resouces.json";
import ResourceTitle from "../components/ResourceTitle";

function Resource() {
  return (
    <div>
      <div className="container-fluid">
        <div className="px-lg-5">
          <div className="container">
            <div className="col-lg-12 mb-1 mb-sm-0">
              <span class="section-title text-primary mb-3 mb-sm-4">
                Resource
              </span>
            </div>
            {ResourceData.data.map((r, i) => {
              return <ResourceTitle resources={r} key={i} />;
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Resource;
