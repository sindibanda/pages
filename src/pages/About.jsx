import React from "react";

function About() {
  return (
    <section id="about" class="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left">
            <img
              src={`${process.env.PUBLIC_URL}/images/about.png`}
              class="img-fluid"
              alt=""
            />
          </div>
          <div
            class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content"
            data-aos="fade-right"
          >
            <h3>
              CEADS (Computational Engineering And Data Science) Research
              Laboratory
            </h3>
            {/* <p class="fst-italic">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
              </p> */}
            <ul>
              <li>
                <i class="bi bi-check-circle"></i> Directed by Dr. Leslie Kerby
              </li>
              <li>
                <i class="bi bi-check-circle"></i> Associate Professor of
                Computer Science
              </li>
              <li>
                <i class="bi bi-check-circle"></i> Affiliate Faculty in Nuclear
                Engineering
              </li>
              <li>
                <i class="bi bi-check-circle"></i> Idaho State University
              </li>
            </ul>
            <p>
              CEADS research and capabilities center around designing, building,
              and securing data-driven modeling and simulation software within
              science and engineering. Projects are varied and include
              scientific machine learning applied to nuclear reactor operation
              and monitoring, scientific machine learning applied to Li-ion
              battery performance and quantum chemistry, and the security of
              machine learning systems as applied in nuclear engineering. Other
              projects involving applications of data science, machine learning,
              and artificial intelligence, and computational science are
              welcome.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}

export default About;
