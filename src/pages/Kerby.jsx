import React from "react";

import kerby from "../data/kerby.json";
import KerbyAbout from "../components/KerbyAbout";
function Kerby() {
  const brycenPdf = () => {
    // using Java Script method to get PDF file
    fetch("WendtDissertationFinal.pdf").then((response) => {
      response.blob().then((blob) => {
        // Creating new object of PDF file
        const fileURL = window.URL.createObjectURL(blob);
        // Setting various property values
        let alink = document.createElement("a");
        alink.href = fileURL;
        alink.download = "WendtDissertationFinal.pdf";
        alink.click();
      });
    });
  };

  const chasePdf = () => {
    // using Java Script method to get PDF file
    fetch("gsmThesisFinal.pdf").then((response) => {
      response.blob().then((blob) => {
        // Creating new object of PDF file
        const fileURL = window.URL.createObjectURL(blob);
        // Setting various property values
        let alink = document.createElement("a");
        alink.href = fileURL;
        alink.download = "gsmThesisFinal.pdf";
        alink.click();
      });
    });
  };

  return (
    <div>
      <div className="backGround">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 mb-4 mb-sm-5">
              <div class="card card-style1 border-0">
                <div class="card-body p-1-9 p-sm-2-3 p-md-6 p-lg-7">
                  <div class="row align-items-center">
                    <div class="col-lg-6 mb-4 mb-lg-0">
                      <img
                        src={`${process.env.PUBLIC_URL}/images/Kerby.jpeg`}
                        className="imgCenter"
                        alt="..."
                      />
                    </div>
                    <div class="col-lg-6 px-xl-10">
                      <div class="bg-secondary d-lg-inline-block py-1-9 px-1-9 px-sm-6 mb-1-9 rounded">
                        <h3 class="h2 text-white mb-0">Dr. Leslie Kerby</h3>
                        <span class="text-primary">CEADS Director</span>
                      </div>
                      <ul class="list-unstyled mb-1-9">
                        <li class="mb-2 mb-xl-3 display-28">
                          <span class="display-26 text-primary me-2 font-weight-600">
                            Position
                          </span>
                        </li>
                        <li>CEADS Director</li>
                        <li>Associate Professor, Computer Science</li>
                        <li>Affiliate Faculty, Nuclear Engineering</li>
                        <li>Idaho State University</li>
                        <li>
                          <a
                            className="cv"
                            href="https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/Kerby_CV.pdf"
                          >
                            CV
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 mb-4 mb-sm-5">
              <div>
                <span class="section-title text-primary mb-3 mb-sm-4">
                  About
                </span>
                <p>
                  This effort seeks to identify possible cyber vulnerablities
                  with machine learning and explore how these could be applied
                  to proposed AI applications in nuclear science/engineering. In
                  addition this effort seeks to provide reccomendations and
                  advice to vendors, end-users and policy makers.
                </p>
                {/* {kerby.data.map((d, i) => {
                  return <KerbyAbout data={d} key={i} />;
                })} */}
              </div>
              <div>
                <span class="section-title text-primary mb-3 mb-sm-4">
                  Publications
                </span>

                <p class="mb-1">
                  <i class="bi bi-play-fill"> </i>
                  <a
                    className="kerby-a"
                    href="https://www.sciencedirect.com/science/article/abs/pii/S0029549322000486?via%3Dihub"
                  >
                    Expanded Analysis of Machine Learning Models for Nuclear
                    Transient Identification Using TPOT
                  </a>
                </p>
                <p class="mb-1">
                  <i class="bi bi-play-fill"> </i>
                  <a
                    className="kerby-a"
                    href="https://arxiv.org/pdf/2111.02513.pdf"
                  >
                    Evaluation of Tree-based Regression over Multiple Linear
                    Regression for Non-normally Distributed Data in Battery
                    Performance
                  </a>
                </p>
                <p class="mb-1">
                  <i class="bi bi-play-fill"> </i>
                  <a
                    className="kerby-a"
                    href="https://pubs.aip.org/aip/adv/article/9/6/065107/1030689/Volumetric-spherical-polynomials"
                  >
                    Volumetric Spherical Polynomials
                  </a>
                </p>
              </div>
              {/* S */}
              {/* <div>
                <span class="section-title text-primary mb-3 mb-sm-4">
                  Student Theses
                </span>

                <p class="mb-1">
                  <div>Pedro Mena, PhD, 2022</div>
                  <i class="bi bi-play-fill"> </i>
                  <a
                    className="kerby-a"
                    href="https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/Dissertation_Draft_5_.pdf"
                  >
                    Auto Machine Learning Applications for Nuclear Reactors:
                    Transient Identification, Model Redundancy and Security
                  </a>
                </p>
                <p class="mb-1">
                  <div>Brycen Wendt, PhD, 2018</div>
                  <i class="bi bi-play-fill"> </i>
                  <a className="kerby-a" onClick={brycenPdf}>
                    Functional Expansions Methods: Optimizations,
                    Characterizations,and Multiphysics Practices
                  </a>
                </p>
                <p class="mb-1">
                  <div>Shovan Chowdhury, MS, 2022</div>
                  <i class="bi bi-play-fill"> </i>
                  <a
                    className="kerby-a"
                    href="https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/Shovan_MS_Thesis__final_copy.pdf"
                  >
                    Artificial Intelligence Based Li-ion Battery Diagnostic
                    Platform
                  </a>
                </p>
                <p class="mb-1">
                  <div>Chase Juneau, MS, 2019</div>
                  <i class="bi bi-play-fill"> </i>
                  <a className="kerby-a" onClick={chasePdf}>
                    The Development of the Generalized Spallation Model{" "}
                  </a>
                </p>
                <p class="mb-1">
                  <div>Pedro Mena, MS, 2019</div>
                  <i class="bi bi-play-fill"> </i>
                  <a
                    className="kerby-a"
                    href="https://github.com/LGKerby/Docs/blob/master/Mena_Pedro_MS_Final.pdf"
                  >
                    Reactor Transient Classification Using Machine Learning
                  </a>
                </p>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Kerby;
