import React, { Component } from "react";
import { Routes, Route, Link } from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import Team from "./pages/Team";
import Research from "./pages/Research";
import Resource from "./pages/Resource";

function App() {
  return (
    <body>
      <header id="header" className="d-flex align-items-center">
        <div className="container-fluid d-flex align-items-center justify-content-between">
          <h1 className="logo">
            <a>CEADS Cyber Nuke</a>
          </h1>
          <nav id="navbar" className="navbar">
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>

              <li>
                <Link to="about">About</Link>
              </li>
              <li>
                <Link to="team">Team</Link>
              </li>
              <li>
                <Link to="research">Research</Link>
              </li>
            </ul>
            <i className="bi bi-list mobile-nav-toggle"></i>
          </nav>
        </div>
      </header>

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/team" element={<Team />} />
        <Route path="/research" element={<Research />} />
      </Routes>
    </body>
  );
}

export default App;
