import React from "react";
import ResourceData from "./ResourceData";

function ResourceTitle(props) {
  {
    console.log(props.resources.resources);
  }
  return (
    <div>
      <div className="section-subtitle text-secondary">
        {props.resources.name}
      </div>

      {props?.resources?.resources?.map((r, i) => {
        return <ResourceData resource={r} key={i} />;
      })}
    </div>
  );
}

export default ResourceTitle;
