import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function TeamCard(props) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div className="col-xl-3 col-lg-4 col-md-6 mb-4">
      <div className="bg-white rounded shadow-sm">
        <img
          src={`${process.env.PUBLIC_URL}/images/${props.team.img}`}
          alt=""
          className="img-fluid card-img-top"
        />
        <div className="p-4">
          <h5 className="text-dark text-bold">{props.team.name}</h5>
          <div className="d-grid gap-2">
            <Button variant="secondary" onClick={handleShow}>
              Bio
            </Button>
          </div>
        </div>
      </div>
      {/* modal */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{props.team.name}</Modal.Title>
        </Modal.Header>
        <img
          src={`${process.env.PUBLIC_URL}/images/${props.team.img}`}
          alt=""
          className="img-fluid card-img-top"
        />
        <Modal.Body className="text-black">{props.team.bio}</Modal.Body>
        <Modal.Footer>
          <Button className=" " onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default TeamCard;
