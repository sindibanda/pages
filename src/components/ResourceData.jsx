import React from "react";

function ResourceData(props) {
  {
    console.log(props.resource);
  }
  return (
    <div>
      <p class="mb-1">
        <i class="bi bi-play-fill"> </i>
        <a className="kerby-a" href={props.resource.link}>
          {props.resource.name}
        </a>
      </p>
    </div>
  );
}

export default ResourceData;
