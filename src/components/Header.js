import React from "react";
import { Link } from "react-router-dom";

function Header(props) {
  return (
    <header id="header" className="d-flex align-items-center">
      <div className="container d-flex align-items-center justify-content-between">
        <h1 className="logo">
          <a>CEADS</a>
        </h1>
        <nav id="navbar" className="navbar">
          <ul>
            <li>
              <Link to="/">
                <a className="nav-link scrollto active" href="#hero">
                  Home
                </a>
              </Link>
            </li>
            <li>
              <a class="nav-link scrollto" href="#about">
                About
              </a>
            </li>
            <li>
              <a class="nav-link scrollto" href="#team">
                Team
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}

export default Header;
