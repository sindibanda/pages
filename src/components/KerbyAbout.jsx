import React from "react";

function KerbyAbout(props) {
  return (
    <div>
      <p class="mb-1">
        <i class="bi bi-play-fill"> </i>
        {props.data.contents}
      </p>
    </div>
  );
}

export default KerbyAbout;
