import React from "react";

function ProjectCard(props) {
  return (
    <div class="col-lg-4 mt-4 mt-lg-4 " data-aos="fade-up" data-aos-delay="300">
      <a href={props.project.link}>
        <div class="box ">
          <span>{props.project.id}</span>

          <h4>{props.project.name}</h4>
        </div>
      </a>
    </div>
  );
}

export default ProjectCard;
